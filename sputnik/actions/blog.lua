--
-- Blog actions
--
module(..., package.seeall)

actions = {}

local disqs_tpl = [=[
<div id="disqus_thread"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'rubberbytes'; // required: replace example with your forum shortname

    // The following are highly recommended additional parameters. Remove the slashes in front to use.
    var disqus_identifier = "$id";
    var disqus_url = "http://rubberbytes.nfshost.com/$id";

    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<a href="http://disqus.com" class="dsq-brlink">blog comments powered by <span class="logo-disqus">Disqus</span></a>
]=]


local blog_template = [=[
	 $most_recent
	  
	<h1>Comments</h1>	  
	 $disqs
	<h1>Older posts</h1>
	 <ol>
	 $do_entries[[<li><a href="$url">$name</a></li>]]
      </ol>
]=]

local entry_template = [=[
	$entry_content
	  
	<h1>Comments</h1>	  
	 $disqs
]=]

 
local function pages_in_order(sputnik)
   local pages = sputnik.saci:get_nodes_by_prefix'blog'
   local res = {}
   for id,page in pairs(pages) do
      res[#res+1] = page
   end
   table.sort(res,function(p1,p2) return (p1.creation_time or '') < (p2.creation_time or '') end)
   return res
end

local wiki = require("sputnik.actions.wiki")
function actions.show_blog(node, request, sputnik)
   -- XXX: Falta el cas on no hi ha cap subpàgina...
	-- i documentar una mica això   
   local pages = pages_in_order(sputnik)
   local most_recent = sputnik:get_node(table.remove(pages, 1).id)

   most_recent = sputnik:decorate_node(most_recent)
   most_recent = sputnik:activate_node(most_recent)

   node.title = most_recent.title:gsub('blog/','')
   node.inner_html = cosmo.f(blog_template) {
	  disqs = cosmo.f(disqs_tpl){ id = most_recent.id },
      most_recent = most_recent.actions.show_content(most_recent, request, sputnik),
      do_entries = function()
		      for _, page in ipairs(pages) do
			 cosmo.yield{
			    id = page.id,
                            url = sputnik:make_url(page.id),
			    name = page.title:gsub('blog/',''),
			 }
		      end
		   end
   }

   return node.wrappers.default(node, request, sputnik)
end

function actions.show_blog_entry(node, request, sputnik)
	node.title = node.title:gsub('blog/', '')
	node.inner_html = cosmo.f(entry_template) {
	  disqs = cosmo.f(disqs_tpl){ id = node.id },
      entry_content = node.actions.show_content(node, request, sputnik)
   }

   return node.wrappers.default(node, request, sputnik)
--	return wiki.actions.show(node, request, sputnik)
end

function actions.rss(node, request, sputnik)
   local pages = pages_in_order(sputnik)


   return cosmo.f(node.templates.RSS){
      title= "Recent Updates to Rubberbytes Blog",
      channel_url = "http://" .. sputnik.config.DOMAIN .. sputnik:make_url(node.id),
      items = function() 
                 for i, page in ipairs(pages) do
                    page = sputnik:decorate_node(page)
                    page = sputnik:activate_node(page)

                    local last_edit = sputnik:get_history(page.id, 1)[0]
                    
                    cosmo.yield{
                       link = "http://" .. sputnik.config.DOMAIN .. sputnik:make_url(page.id),
                       title = page.title:gsub('blog/',''),
                       ispermalink = "false",
                       guid = page.id,
                       pub_date    =  sputnik:format_time_RFC822(page.creation_time), --sputnik:format_time_RFC822(last_edit.timestamp),
                       author = "Joan Arnaldich",
                       summary = sputnik:escape(page.actions.show_content(page, request, sputnik))
                    }
                 end
              end
   }, "application/rss+xml"
end

 
function actions.save_page(node, request, sputnik)
   if not node.creation_time then
      local params = {}
      params.author = request.user
      params.creation_time = os.time()
      node = sputnik:update_node_with_params(node, params)
   end
   return node
end


