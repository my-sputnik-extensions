--
-- BLOG HOOK module
--
module(..., package.seeall)

function save_page(node, request, sputnik)
   if not node.creation_time then
      local params = {}
      params.author = request.user
      params.creation_time = tostring(os.time())
      node = sputnik:update_node_with_params(node, params)
   end
   return node
end
