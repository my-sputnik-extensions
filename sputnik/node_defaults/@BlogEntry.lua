module(..., package.seeall)

NODE = {
   content = "",
   category = "prototypes",
   actions= [[
	 edit = "wiki.edit"
	 show = "blog.show_blog_entry"
   ]],
   permissions = [[
	 deny(Anonymous, edit_and_save)
   ]],
   fields = [[
	 author = {1.1}
	 creation_time = {1.2}
	 tags = {1.3}
	 short_desc = {1.4}
   ]],
	edit_ui = [[
		blog_section  = {1.401, "div_start", id="comment_section", closed="true"}
			short_desc = {1.402, "textarea", rows=15, no_label=false, editor_modules = {"resizeable"}}
			content        = {1.403, "textarea", rows=15, no_label=true, editor_modules = {"resizeable"}}
		blog_section_end = {1.404, "div_end"}
	]],
   save_hook = "blog.save_page"
}

NODE.html_head = [==[
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="keywords" content="$html_meta_keywords"/>
  <meta name="description" content="$html_meta_description"/>
  <title>$site_title: $title</title>
  <link type="text/css" rel="stylesheet" href="$css_base_url{}sputnik/style.css" media="all"/>
  $do_css_links[[<link type="text/css" rel="stylesheet" href="$href" media="$media"/>
]]$do_css_snippets[[
   <style type="text/css" media="$media">$snippet</style>
]]
<link rel="shortcut icon" href="$favicon_url"/>
  <link rel="alternate" type="application/rss+xml" title="_(RECENT_EDITS_TO_NODE)" $node_rss_link/>
  $if_no_index[[<meta name="ROBOTS" content="NOINDEX, NOFOLLOW"/>
]]]==]

NODE.html_header = [===[
    <div id="login" style="vertical-align: middle;">
     <!--login and search (in the upper right corner) -->
     $if_search[[$search]]<br/><br/>
   </div>

   <div id="menu_bar">
$menu<!--br/><br/-->
   </div>
]===]
