module(..., package.seeall)

NODE = {
   title = "Blog",
   content = "",
   child_defaults = [[
	 any='prototype="@BlogEntry"'
   ]],
   actions = [[
	 show = "blog.show_blog"
	 reload = "wiki.reload"
         rss = "blog.rss"
   ]],
}

NODE.html_head = [==[
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="keywords" content="$html_meta_keywords"/>
  <meta name="description" content="$html_meta_description"/>
  <title>$site_title: $title</title>
  <link type="text/css" rel="stylesheet" href="$css_base_url{}sputnik/style.css" media="all"/>
  $do_css_links[[<link type="text/css" rel="stylesheet" href="$href" media="$media"/>
]]$do_css_snippets[[
   <style type="text/css" media="$media">$snippet</style>
]]
<link rel="shortcut icon" href="$favicon_url"/>
  <link rel="alternate" type="application/rss+xml" title="_(RECENT_EDITS_TO_NODE)" $node_rss_link/>
  $if_no_index[[<meta name="ROBOTS" content="NOINDEX, NOFOLLOW"/>
]]]==]

NODE.html_header = [===[
    <div id="login" style="vertical-align: middle;">
     <!--login and search (in the upper right corner) -->
     $if_search[[$search]]<br/><br/>
   </div>

   <div id="menu_bar">
$menu<!--br/><br/-->
   </div>
]===]

